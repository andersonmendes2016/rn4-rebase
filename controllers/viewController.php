<?php
class viewController extends controller {

	public function index() {

	}
	public function rom($id) {
		$id = base64_decode($id);
		$data = array();
		$r = new roms();
		$data['rom'] = $r->getbuildbyid($id);
		$data['page'] = $data['rom']['name'];
		$data['session'] = 'roms';
		$this->loadTemplate('view', $data);
	}

	public function kernel($id) {
		$id = base64_decode($id);
		$data = array();
		$k = new kernels();
		$data['rom'] = $k->getbuildbyid($id);
		$data['page'] = $data['rom']['name'];
		$data['session'] = 'kernels';
		$this->loadTemplate('view', $data);
	}

	public function other($id) {
		$id = base64_decode($id);
		$data = array();
		$o = new others();
		$data['rom'] = $o->getbuildbyid($id);
		$data['page'] = $data['rom']['name'];
		$data['session'] = 'others';
		$this->loadTemplate('view', $data);
	}
}