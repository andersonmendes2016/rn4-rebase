<?php

class romsController extends controller {
	
	public function index() {
		$data = array();
		$data['page'] = "All ROMs";
		$data['session'] = 'roms';
		$this->loadTemplate('roms',$data);
	}

	public function aosp() {
		$data = array();
		$data['page'] = "AOSP Roms";
		$n = new names();
		$data['roms'] = $n->listSub("rom");
		$data['session'] = 'roms';
		$this->loadTemplate('builds',$data);
	}

	public function miui() {
		$data = array();
		$data['page'] = "MIUI Stock";
		$n = new names();
		$data['roms'] = $n->listSub("stock");
		$data['session'] = 'roms';
		$this->loadTemplate('builds',$data);
	}

	public function miuicustom() {
		$data = array();
		$data['page'] = "MIUI Custom";
		$n = new names();
		$data['roms'] = $n->listSub("custom");
		$data['session'] = 'roms';
		$this->loadTemplate('builds',$data);
	}

	public function builds($cod,$dev='') {
		$data = array();
		$r = new roms();
		$n = new names();
		if(!empty($dev)){
			$data['updates'] = $r->getUpdatesbyCodDev($cod,$dev);
		}else{
			$data['updates'] = $r->getUpdatesbyCod($cod);
		}
		$data['cod'] = $cod;
		$data['page'] = $n->getNameByCod($cod)." Builds";
		$data['multidev'] = $r->getDevs($cod);
		$data['session'] = 'roms';
		$this->loadTemplate('build',$data);
	}
}