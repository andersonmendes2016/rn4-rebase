<?php
class others extends model {

	public function getUpdatesbyCod($cod) {
		$data = array();

		$sql = "SELECT * FROM others WHERE othertype = '$cod' ORDER BY id DESC";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function push($name, $source, $changelog, $othertype, $link) {
		if(!empty($name) && !empty($link) && !empty($othertype)) {
			$name = addslashes($name);
			$source = addslashes($source);
			$changelog = addslashes($changelog);
			$othertype = addslashes($othertype);
			$link = addslashes($link);

			$sql = "INSERT INTO others SET 
			name = '$name', source = '$source', changelog = '$changelog', othertype = '$othertype', link = '$link'";

			$this->db->query($sql);

			header("Location: /");
		} else {
			header("Location /error");
		}
	}

	public function getUpdates($l) {
		$data = array();

		$sql = "SELECT * FROM others ORDER BY id DESC LIMIT $l";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function drop($id) {
		if(!empty($id)){
			$id = addslashes($id);
			$sql = "DELETE FROM others WHERE id = '$id'";
			$this->db->query($sql);
		}
	}

	public function getbuildbyid($id) {
		$data = array();
		if(isset($id) && !empty($id)){
			$id = addslashes($id);

			$sql = "SELECT * FROM others WHERE id = '$id'";
			$sql = $this->db->query($sql);
			if($sql->rowCount() > 0){
				$data = $sql->fetch();
			}
		return $data;
		}
	}

	public function edit($id, $name, $source, $changelog, $othertype, $link) {
		if(!empty($name) && !empty($link) && !empty($othertype)) {
			$name = addslashes($name);
			$source = addslashes($source);
			$changelog = addslashes($changelog);
			$othertype = addslashes($othertype);
			$link = addslashes($link);

			$sql = "UPDATE others SET 
			name = '$name', source = '$source',	changelog = '$changelog', othertype = '$othertype', link = '$link' WHERE id = '$id'";
			$this->db->query($sql);

			header("Location: /");
		} else {
			header("Location /error");
		}
	}

}