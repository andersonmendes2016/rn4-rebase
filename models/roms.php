<?php
class roms extends model {

	public function getUpdatesbyCod($cod) {
		$data = array();

		$sql = "SELECT * FROM roms WHERE romtype = '$cod' ORDER BY id DESC";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function getUpdatesbyCodDev($cod, $dev) {
		$data = array();

		$sql = "SELECT * FROM roms WHERE romtype = '$cod' AND dev = '$dev' ORDER BY id DESC";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}else{
			header("Location: /");
		}
		return $data;
	}

	public function pushRom($name, $dev, $source, $link, $changelog, $romtype) {
		if(!empty($name) && !empty($link) && !empty($romtype)) {
			$name = addslashes($name);
			$dev = addslashes($dev);
			$source = addslashes($source);
			$link = addslashes($link);
			$changelog = addslashes($changelog);
			$romtype = addslashes($romtype);

			$sql = "INSERT INTO roms SET 
			name = '$name', dev = '$dev', source = '$source', link = '$link', 
			changelog = '$changelog', romtype = '$romtype', date = NOW()";

			$this->db->query($sql);

			header("Location: /");
		} else {
			header("Location /error");
		}
	}

	public function getUpdates($l) {
		$data = array();

		$sql = "SELECT * FROM roms ORDER BY id DESC LIMIT $l";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function getUpdatesbyType($t,$l) {
		$data = array();
		$d = array();
		$a = array();

		$s = "SELECT cod FROM names WHERE subtype = '$t' ORDER BY id DESC LIMIT $l";
		$s = $this->db->query($s);

		if($s->rowCount() > 0 ){
			$d = $s->fetchAll();

			foreach ($d as $key => $value) {
				$a[] = $value['cod'];
			}

			$sql = 'SELECT * FROM roms WHERE romtype IN ("'.implode('", "', $a).'") ORDER BY id DESC LIMIT '.$l;
			$sql = $this->db->query($sql);
			if($sql->rowCount() > 0) {
				$data = $sql->fetchAll();
			}

			return $data;
		}
	}

	public function drop($id) {
		if(!empty($id)){
			$id = addslashes($id);
			$sql = "DELETE FROM roms WHERE id = '$id'";
			$this->db->query($sql);
		}
	}

	public function edit($id, $name, $dev, $source, $link, $changelog, $romtype) {
		if(!empty($name) && !empty($link) && !empty($romtype)) {
			$name = addslashes($name);
			$dev = addslashes($dev);
			$source = addslashes($source);
			$link = addslashes($link);
			$changelog = addslashes($changelog);
			$romtype = addslashes($romtype);

			$sql = "UPDATE roms SET 
			name = '$name', dev = '$dev', source = '$source', link = '$link', 
			changelog = '$changelog', romtype = '$romtype' WHERE id = '$id'";

			$this->db->query($sql);

			header("Location: /");
		} else {
			header("Location /error");
		}
	}

	public function getbuildbyid($id) {
		$data = array();
		if(isset($id) && !empty($id)){
			$id = addslashes($id);
			$sql = "SELECT * FROM roms WHERE id = '$id'";
			$sql = $this->db->query($sql);
			if($sql->rowCount() > 0){
				$data = $sql->fetch();
			}else {
				header("Location: /");
			}
		}

		return $data;
	}

	public function getDevs($c) {
		$data = array();
		if(isset($c) && !empty($c)) {

			$sql = "SELECT DISTINCT dev FROM roms WHERE romtype = '$c'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() > 1){
				$data = $sql->fetchAll();
			}

		}
	return $data;
	}
}