<?php
class kernels extends model {

	public function getUpdatesbyCod($cod) {
		$data = array();

		$sql = "SELECT * FROM kernels WHERE kerneltype = '$cod' ORDER BY id DESC";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function pushKernel($name, $dev, $source, $link, $changelog, $kerneltype) {
		if(!empty($name) && !empty($link) && !empty($kerneltype)) {
			$name = addslashes($name);
			$dev = addslashes($dev);
			$source = addslashes($source);
			$link = addslashes($link);
			$changelog = addslashes($changelog);
			$kerneltype = addslashes($kerneltype);

			$sql = "INSERT INTO kernels SET 
			name = '$name', dev = '$dev', source = '$source', link = '$link', 
			changelog = '$changelog', kerneltype = '$kerneltype', date = NOW()";
			$sql = $this->db->query($sql);
			header("Location: /");
		} else {
			header("Location /error");
		}
	}

	public function getUpdatesbyType($t,$l) {
		$data = array();
		$d = array();
		$a = array();

		$s = "SELECT cod FROM names WHERE subtype = '$t' ORDER BY id DESC LIMIT $l";
		$s = $this->db->query($s);

		if($s->rowCount() > 0 ){
			$d = $s->fetchAll();

			foreach ($d as $key => $value) {
				$a[] = $value['cod'];
			}

			$sql = 'SELECT * FROM kernels WHERE kerneltype IN ("'.implode('", "', $a).'") ORDER BY id DESC LIMIT '.$l;
			$sql = $this->db->query($sql);
			if($sql->rowCount() > 0) {
				$data = $sql->fetchAll();
			}

			return $data;
		}
	}

	public function getUpdates($l) {
		$data = array();

		$sql = "SELECT * FROM kernels ORDER BY id DESC LIMIT $l";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function drop($id) {
		if(!empty($id)){
			$id = addslashes($id);
			$sql = "DELETE FROM kernels WHERE id = '$id'";
			$this->db->query($sql);
		}
	}

	public function getbuildbyid($id) {
		$data = array();
		if(isset($id) && !empty($id)){
			$id = addslashes($id);

			$sql = "SELECT * FROM kernels WHERE id = '$id'";
			$sql = $this->db->query($sql);
			if($sql->rowCount() > 0){
				$data = $sql->fetch();
			}
		}

		return $data;
	}
	public function edit($id, $name, $dev, $source, $link, $changelog, $kerneltype) {
		if(!empty($name) && !empty($link) && !empty($kerneltype)) {
			$name = addslashes($name);
			$dev = addslashes($dev);
			$source = addslashes($source);
			$link = addslashes($link);
			$changelog = addslashes($changelog);
			$kerneltype = addslashes($kerneltype);

			$sql = "UPDATE kernels SET 
			name = '$name', dev = '$dev', source = '$source', link = '$link', 
			changelog = '$changelog', kerneltype = '$kerneltype' WHERE id = '$id'";
			$this->db->query($sql);

			header("Location: /");
		} else {
			header("Location /error");
		}
	}
}