<div class='build dpage' data-pg='true'>
	<?php if(!empty($rom['dev'])) :?>
	<div class='dev'>by <?php echo $rom['dev']; ?></div>
	<?php endif; ?>
	<?php if(!empty($rom['changelog'])) :?>
	<div class='desc'>
<pre>Description:

	<?php echo $rom['changelog']; ?>
</pre>
	</div>
<?php endif; ?>
</div>
<a href="<?php echo $rom['link']; ?>" class="download"><button type="button" class="btn btn-view btn-lg btn-block ">Download</button></a>
<?php if(!empty($rom['source'])) :?>
<a href="<?php echo $rom['source']; ?>"><button type="button" class="btn btn-view btn-lg btn-block" >Source</button></a>
<?php endif; ?>
<a class="direct"><button type="button" class="btn btn-view btn-lg btn-block btnd" >Direct link (beta)</button></a>
<a onclick="history.go(-1)"><button type="button" class="btn btn-view btn-lg btn-block back" >Back</button></a>
</br>