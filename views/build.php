<?php if(!empty($multidev)) : ?>
 <div class="card">
  <div class="card-body">
    <h6>Filter build by developer:</h6>
    <?php foreach ($multidev as $dev ) : ?>
    <a href="<?php echo "/".$session."/builds/".$cod."/".$dev[0] ?>"><button type="button" class="btn btn-secondary"><?php echo $dev[0] ?></button></a>
  <?php endforeach; ?>
  </div>
</div>
</br>
<?php endif; ?>

<?php
$link = '/view/rom/';
if(isset($session) && $session != 'home'){
  if($session == 'kernels'){
    $link = '/view/kernel/';
  }elseif($session == 'others'){
    $link = '/view/other/';
  }
}
?>
<?php foreach ($updates as $update): ?>
<div class="card">
  <div class="card-body">
  <a href="<?php echo $link.base64_encode($update['id']) ?>">
    <h6 class="card-title">
      <?php 
      echo $update['name'];
    if(!empty($update['dev'])){
      echo " by ".$update['dev'];}
      ?>
    </h6></a>
    <a href="<?php echo $update['link'] ?>"><button type="button" class="btn btn-secondary">Download</button></a>
    <a href="<?php echo $link.base64_encode($update['id']) ?>"><button type="button" class="btn btn-secondary">More</button></a>
  </div>
</div>
<?php endforeach; ?>
</br>
