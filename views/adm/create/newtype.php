<style type="text/css">
	h4 {
	color: white;
	margin: 10px;
	}
</style>
<?php if (isset($types) && !empty($types)): ?>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Codname</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($types as $type): ?>
    <tr>
      <th><?php echo $type['id'] ?></th>
      <td><?php echo $type['name'] ?></td>
      <td><?php echo $type['cod'] ?></td>
    </tr>
<?php endforeach; ?>
<?php endif; ?>
  </tbody>
</table>
</br>
<h4>Add a new:</h4>
<form method="POST">
	<div class="form-group">
	<label>Name:</label>
	<input type="text" class="form-control" name="name" placeholder="nome">
	<label >Codename:</label>
	<input type="text" class="form-control" name="codename" placeholder="codename">
	</div>
	<button class="btn btn-secondary" id="push">PUSH</button>
</form>