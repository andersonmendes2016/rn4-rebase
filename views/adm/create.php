<style type="text/css">
  a button {
    margin-top:8px;
  }
  a:hover {
    text-decoration: none;
  }
</style>
<div class="alert alert-dark warn" role="alert">ROMS</div>
<div class="btns">
<a href="/adm/create/romaosp"><button type="button" class="btn btn-secondary btn-lg btn-block" >Create Rom - AOSP</button></a>
<a href="/adm/create/rommiuic"><button type="button" class="btn btn-secondary btn-lg btn-block" >Create Rom - MIUI CUSTOM</button></a>
<a href="/adm/create/rommiui"><button type="button" class="btn btn-secondary btn-lg btn-block" >Create Rom - MIUI STOCK</button></a>
</br>
<a href="/adm/create/romtypeaosp"><button type="button" class="btn btn-secondary btn-lg btn-block" >Create RomType - AOSP</button></a>
<a href="/adm/create/romtypemiuic"><button type="button" class="btn btn-secondary btn-lg btn-block" >Create RomType - MIUI CUSTOM</button></a>
<a href="/adm/create/romtypemiui"><button type="button" class="btn btn-secondary btn-lg btn-block" >Create RomType - MIUI STOCK</button></a>
<div class="alert alert-dark warn" role="alert">KERNELS</div>
<a href="/adm/create/kernel"><button type="button" class="btn btn-secondary btn-lg btn-block">Create Kernel - AOSP</button></a>
<a href="/adm/create/kernelmiui"><button type="button" class="btn btn-secondary btn-lg btn-block">Create Kernel - MIUI</button></a>
</br>
<a href="/adm/create/kerneltype"><button type="button" class="btn btn-secondary btn-lg btn-block">Create KernelType - AOSP</button></a>
<a href="/adm/create/kerneltypemiui"><button type="button" class="btn btn-secondary btn-lg btn-block">Create KernelType - MIUI</button></a>
<div class="alert alert-dark warn" role="alert">OTHERS</div>
<a href="/adm/create/other"><button type="button" class="btn btn-secondary btn-lg btn-block">Create Other</button></a>
</br>
<a href="/adm/create/othertype"><button type="button" class="btn btn-secondary btn-lg btn-block">Create OtherType</button></a>
</br>
</div>