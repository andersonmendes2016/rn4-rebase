<form method="POST">
  <div class="form-group">
    <label for="exampleFormControlInput1">Link</label>
    <input value="<?php echo $other['link'] ?>" type="link" class="form-control" name="link" id="link" placeholder="https://example.com">

    <label for="exampleFormControlInput1">Name</label>
    <input value="<?php echo $other['name'] ?>" type="text" class="form-control" id="name" placeholder="Kernel_bdkdfbsefjsnf.zip" name="name">
 
     <label for="exampleFormControlSelect1">SubType</label>
    <select class="form-control" id="exampleFormControlSelect1" name="othertype">
      <?php foreach ($types as $type): ?>
      <option <?php if($type['cod'] == $other['othertype']) {echo 'value="'.$type['cod'].'" selected ';}else{echo 'value="'.$type['cod'].'"';}?>><?php echo $type['name'] ?></option>
    <?php endforeach;?>
    </select>
    <label for="exampleFormControlInput1">Source</label>
    <input value="<?php echo $other['source'] ?>"  type="text" class="form-control" placeholder="xda" name="source">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Changelog or Description</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="changelog"><?php echo $other['changelog'] ?></textarea>
  </div>
      <button class="btn btn-secondary" id="push">PUSH</button>
</form>