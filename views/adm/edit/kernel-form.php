<form method="POST">
  <div class="form-group">
    <label for="exampleFormControlInput1">Link</label>
    <input value="<?php echo $kernel['link'] ?>" type="link" class="form-control" name="link" id="link" placeholder="https://example.com">

    <label for="exampleFormControlInput1">Build name</label>
    <input value="<?php echo $kernel['name'] ?>" type="text" class="form-control" id="name" placeholder="Kernel_bdkdfbsefjsnf.zip" name="name">
    <label for="exampleFormControlInput1">Developer</label>
    <input value="<?php echo $kernel['dev'] ?>" type="text" class="form-control" id="dev" placeholder="Developer" name="dev">
     <label for="exampleFormControlSelect1">SubType</label>
    <select class="form-control" id="exampleFormControlSelect1" name="kerneltype">
      <?php foreach ($types as $type): ?>
      <option <?php if($type['cod'] == $kernel['kerneltype']) {echo 'value="'.$type['cod'].'" selected ';}else{echo 'value="'.$type['cod'].'"';}?>><?php echo $type['name'] ?></option>
    <?php endforeach;?>
    </select>
    <label for="exampleFormControlInput1">Source</label>
    <input value="<?php echo $kernel['source'] ?>"  type="text" class="form-control" placeholder="xda" name="source">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Changelog or Description</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="changelog"><?php echo $kernel['changelog'] ?></textarea>
  </div>
      <button class="btn btn-secondary" id="push">PUSH</button>
</form>