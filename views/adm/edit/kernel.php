<?php if (isset($kernels) && !empty($kernels)): ?>
<h3 style="color: white; margin: 10px;">Latest Builds</h3>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Date</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($kernels as $kernel): ?>
    <tr>
      <th><?php echo $kernel['id'] ?></th>
      <td><?php echo $kernel['name'] ?></td>
      <td><?php echo $kernel['date'] ?></td>
      <td><a href="/adm/editkernel/<?php echo $kernel['id']."/".$typer ?>"><button type="button" class="btn btn-warning">Edit</button></a></td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
