<?php
$link = '/view/rom/';
if(isset($session) && $session != 'home'){
  if($session == 'kernels'){
    $link = '/view/kernel/';
  }elseif($session == 'others'){
    $link = '/view/other/';
  }
}
?>
<?php foreach ($updates as $update): ?>
<?php 
$msg = "";
$msg = $update['name'];
$msg .= " - ".$update['date'];
if(!empty($update['dev'])){
  $msg .= " - by ".$update['dev'];
}
?>

<div class="card">
  <div class="card-header">
    <?php echo $msg; ?>
  </div>
  <div class="card-body">
  <a href="<?php echo $link.base64_encode($update[0]) ?>">
    <h6 class="card-title">
      <?php 
      echo $update[1];?>
    </h6></a>
    <a href="<?php echo $update['link'] ?>"><button type="button" class="btn btn-secondary">Download</button></a>
    <a href="<?php echo $link.base64_encode($update[0]) ?>"><button type="button" class="btn btn-secondary">More</button></a>
  </div>
</div>
<?php endforeach; ?>
</br>
