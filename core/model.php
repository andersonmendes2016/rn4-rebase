<?php
class model {

	protected $db;

	public function __construct(){
		global $config;
		try{
			$this->db = new PDO("mysql:dbname=".$config['dbname'].";host=".$config['host'], $config['dbuser'], $config['dbpass']);
		} catch (Exception $e) {
   			 echo 'Failed to acess DB: ',  $e->getMessage(), "\n";
   			 die();
		}
	}
}